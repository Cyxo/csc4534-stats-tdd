from pytest import approx, fixture
from stats import Module

@fixture
def example_module():
    m = Module()
    m.add_student("Durden", "Tyler", [15, 10, 12])
    m.add_student("Singer", "Marla", [12, 14, 5])
    m.add_student("Paulson", "Bob", [5, 6, 10])
    m.add_student("Face", "Angel", [12, 20, 14])
    return m

def test_nb_eleves(example_module):
    assert example_module.n_students() == 4
    
def test_overall_grade(example_module):
    assert example_module.overall_grade("Durden", "Tyler") == approx(12.33, 0.01)
    assert example_module.overall_grade("Singer", "Marla") == approx(10.33, 0.01)
    assert example_module.overall_grade("Paulson", "Bob") == approx(7.00, 0.01)
    assert example_module.overall_grade("Face", "Angel") == approx(15.33, 0.01)
